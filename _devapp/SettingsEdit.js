import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

import Button from 'devextreme-react/button';
import TextBox from 'devextreme-react/text-box';
import SelectBox from 'devextreme-react/text-box';
import dxForm from "devextreme/ui/form";
import Form from 'devextreme-react/form';
import Instances from './Instances.js';

export default class SettingsEdit extends Component {

    formItems = [
        { dataField: "name", caption: "Name" },
        { dataField: "title", caption: "title" },
        { dataField: "domain", caption: "domain" },
        { dataField: "googleClient", caption: "" },
        { dataField: "googleKey", caption: "" },
        { dataField: "googleAnalyticsId", caption: "" },
        { dataField: "googleAnalyticsUrl", caption: "" },
        { dataField: "googleMapClient", caption: "" },
        { dataField: "googleMapKey", caption: "" },
        { dataField: "googleApiKey", caption: "" },
        { dataField: "googleAPI_FrontendKey", caption: "" },
        { dataField: "googleAPI_BackendKey", caption: "" }
    ];

    constructor(props) {
        super(props);
        var Instance = null;
        Instances.forEach((__Instance) => {
            if (__Instance.name === props.match.params.instance) {
                Instance = __Instance;
                return false;
            }
        })
        this.state = {
            Settings: {},
            Instance: Instance,
            blocked: true
        };
        this.handleChange = this.handleChange.bind(this);
        this.saveSettings = this.saveSettings.bind(this);
        this.loadSettings = this.loadSettings.bind(this);
        this.getUrl = this.getUrl.bind(this);
    }

    render() {
        return (
            <div>
                <div className="col-50">
                    <div>
                        <Button
                            onClick={() => {
                                window.location = "/";
                            }}
                            type="danger"
                            text="Go back to instances list"
                            width="100%"
                        />
                    </div>
                    <Form
                        formData={this.state.Settings}
                        onFieldDataChanged={this.handleChange}
                        labelLocation='top'
                        items={this.formItems}
                        disabled={this.state.blocked}
                    />
                    <hr />
                    <div>
                        {this.renderButtons()}
                    </div>
                </div>
                <div className="col-50" style={{height: "300px", overflow: "scroll"}}>
                    <pre>{JSON.stringify(this.state, undefined, 2)}</pre>
                </div>
                <div className="clear clearfix"></div>
            </div>
        )
    }

    renderButtons() {
        return (
            <Button
                onClick={this.saveSettings}
                type="success"
                disabled={this.state.blocked}
                text="Save Settings @ Instance"
            />
        )
    }

    getUrl() {
        return "https://" + this.state.Instance.url + "/settings/settings?u=padminp2&p=" + '{!ooL+-y0"9-s}6';
    }

    loadSettings() {
        var url = this.getUrl();
        fetch(url, {
            method: 'GET',
            mode: 'cors',
            header: {
              accept: "application/json"
            },
            //credentials: 'include'
        }).then(result => {
            return result.json();
        }).then(data => {
          console.log(data);
            var state = this.state;
            var settings = {};
            // Populate with data from API.
            data.forEach(opt => { settings[opt.option_name] = opt.option_value; });
            state.Settings = settings;
            state.blocked = false;
            this.setState(state);
        }).catch(err => {
          console.log(err);
            //alert(err);
        })
    }

    saveSettings() {
        this.setState({ blocked: true });
        var Settings = {};
        this.formItems.forEach(f => { Settings[f.dataField] = this.state.Settings[f.dataField]; });
        var body = JSON.stringify({ Settings: Settings });
        var url = this.getUrl();
        var reload = this.loadSettings;
        fetch(url, {
            method: 'POST',
            body: body
        }).then(result => {
            return result.json();
        }).then(data => {
            setTimeout(() => {
                reload();
            }, 1000);
        });
    }

    handleChange(e) {
        this.setState({}); // __TODO__ how does this work??? devexpress handles this?
    }

    componentDidMount() {
        this.loadSettings();
    }

}
