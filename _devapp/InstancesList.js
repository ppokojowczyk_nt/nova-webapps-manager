import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import Button from 'devextreme-react/button';
import DataGrid from 'devextreme-react/data-grid';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';
import { Center } from 'devextreme-react/map';
import { DX_REMOVE_EVENT } from 'devextreme-react/core/component-base';
import Instances from './Instances.js';
import { withRouter } from 'react-router-dom'

export default class InstancesList extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const columns = [
            { caption: "Name", dataField: "name" },
            { caption: "URL", dataField: "url" },
            { caption: "Host", dataField: "host" },
            {
                caption: "Status", dataField: "status", cellTemplate: (c, e) => {
                    c.innerHTML = e.value || "Unknown";
                }
            },
            { caption : "Options", cellTemplate: (c,e) => {
                render(<Button text="Settings"
                type="success"
                onClick={()=>{
                    this.editSettings(e.data);
                }}
                />, c);
            }}
        ];

        return (
            <DataGrid
                searchPanel={{ enabled: true }}
                dataSource={Instances}
                columns={columns}
                editing={null}
                paging={null}
                height="100%"
                onInitialized={this.onGridInitialized}
            />
        )
    }

    editSettings(Instance){
        window.location = "/settings/" + Instance.name;
    }

    onGridInitialized(e) {
    }


    componentDidMount() {
    }
}
