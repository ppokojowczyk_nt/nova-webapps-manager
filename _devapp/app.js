import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import { asyncComponent } from 'react-async-component';

import AppHeader from './AppHeader.js';
import SettingsEdit from './SettingsEdit.js';
import InstancesList from './InstancesList.js';

import './css/app.css';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.compact.css';

import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';

/* globals __webpack_public_path__ */
__webpack_public_path__ = `${window.STATIC_URL}/assets/bundle/`;

class Myapp extends Component {
    render() {
        return (
            <Fragment>
                <div className="container">
                    <AppHeader />
                </div>
                {/* <div className="container">
                    <InstancesList/>
                </div> */}
                <Router>
                    <div className="container container--rest">
                        <Route exact path="/" component={InstancesList} />
                        <Route path="/settings/:instance" component={SettingsEdit} />
                    </div>
                </Router>
            </Fragment>
        )
    }
}

render(<Myapp />, document.getElementById('app'));
